<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Interfaces;

interface TransportObjectInterface
{
    /**
     * Sprawdza, czy TO jest prawidłowy
     *
     * @return bool
     */
    public function isValid(): bool;

    /**
     * Zamienia własności obiektu na tablicę
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Zamienia własności obiektu na StdClass
     *
     * @return \StdClass
     */
    public function toStdClass(): \StdClass;

    /**
     * Zamienia własności obiektu na Jsona
     *
     * @return string
     */
    public function toJson(): string;

    /**
     * Przygotowuje obiekt ze wstrzykniętej tablicy
     *
     * @param array $params
     *
     * @return TransportObjectInterface
     */
    public function prepareFromArray(array $params): TransportObjectInterface;
}
