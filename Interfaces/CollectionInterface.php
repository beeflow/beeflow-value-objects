<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 05.03.18
 * Time: 12:51
 */

namespace  Beeflow\ValueObject\Interfaces;

use Beeflow\ValueObject\Exceptions\InvalidKeyException;
use Beeflow\ValueObject\Exceptions\KeyInUseException;

interface CollectionInterface extends \Countable, \IteratorAggregate, \ArrayAccess
{
    /**
     * Dodawanie elementu do kolekcji
     *
     * @param      $obj
     * @param null $key
     *
     * @return CollectionInterface
     * @throws KeyInUseException
     * @throws \TypeError
     */
    public function add($obj, $key = null);

    /**
     * @param $item
     * @param $key
     *
     * @return mixed
     * @throws \TypeError
     */
    public function set($item, $key);

    /**
     * Usuwanie elementu z kolekcji
     *
     * @param $key
     *
     * @return CollectionInterface
     * @throws InvalidKeyException
     */
    public function remove($key);

    /**
     * Pobieranie elementu z kolekcji
     *
     * @param $key
     *
     * @return mixed
     * @throws InvalidKeyException
     */
    public function get($key);

    /**
     * @return array
     */
    public function keys(): array;

    /**
     * @return int
     */
    public function length(): int;

    /**
     * @param $key
     *
     * @return bool
     */
    public function keyExists($key): bool;

    /**
     * @return array
     */
    public function toArray(): array;
}
