<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Interfaces;

use Beeflow\ValueObject\Exceptions\CastException;

interface ValueObjectInterface
{
    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString();

    /**
     * @return mixed
     */
    public function get();

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int;
}
