#!/bin/bash
cwd=`pwd`
echo "" > phpunitresult

printf "Sprawdzam, czy wszystkie wymagane narzędzia są zainstalowane\n";
printf "   PHPCodingStyle "
if [ -d `whereis phpcs | cut -d':' -f 2` ];
then
    printf "[ FALSE ]\n";
    printf "Instalacja PHPCodingStyle\n";
    sudo pear install PHP_CodeSniffer;
else
    printf "[ OK ]\n";
fi;

printf "   PHPMesDetector "
if [ -d `whereis phpmd | cut -d':' -f 2` ];
then
    printf "[ FALSE ]\n";
    printf "Instalacja PHPMesDetector\n";
    wget -c http://static.phpmd.org/php/latest/phpmd.phar && chmod +x phpmd.phar && sudo mv phpmd.phar /usr/bin/phpmd
else
    printf "[ OK ]\n";
fi;

printf "   PHPUnit 6 "
if [ -d `whereis phpunit | cut -d':' -f 2` ];
then
    printf "[ FALSE ]\n";
    printf "Instalacja PHPUnit 6\n";
    wget -O phpunit https://phar.phpunit.de/phpunit-6.phar && chmod +x phpunit && sudo mv phpunit /usr/bin/phpunit
else
    printf "[ OK ]\n";
fi;

phpCheckCodeSyntax ()
{
    start=$(date +%s)
    printf  "Rozpoczynam sprawdzanie składni kodu za pomocą wywołania skryptu php -l w katalogu: $1\n";
    files=$(find $1/ -type f -name \*.php)
    for f in $files
    do
        if [ -f "$f" ]
        then
            php -l $f | grep 'Errors parsing' >> phpunitresult
        fi
    done
    printf  "\nCzas trwania operacji: $(date -d @$(($(date +%s)-$start)) +"%M minut %S sekund")\n\n"
}

phpCheckCodeForPSR2 ()
{
    start=$(date +%s)
    printf  "\nRozpoczynam poprawianie składni kodu za pomocą wywołania skryptu phpcbf w katalogu: $1\n";
    phpcbf $1 --standard=PSR2 --extensions=php --error-severity=1 --warning-severity=8

    printf  "Rozpoczynam sprawdzanie składni kodu za pomocą wywołania skryptu phpcs w katalogu: $1\n";
    phpcs $1 --standard=PSR2 --extensions=php --error-severity=1 --warning-severity=8 | tee -a phpunitresult
    printf  "\nCzas trwania operacji: $(date -d @$(($(date +%s)-$start)) +"%M minut %S sekund")\n\n"
}

phpCheckCodeWithMessDetector ()
{
    start=$(date +%s)
    printf  "Rozpoczynam sprawdzanie składni kodu za pomocą wywołania skryptu phpmd w katalogu: $1\n";
    phpmd $1 text $cwd/phpmd.xml | tee -a phpmdresult
    printf  "\nCzas trwania operacji: $(date -d @$(($(date +%s)-$start)) +"%M minut %S sekund")\n\n"
}

if [ "$#" -eq  "0" ]
then
    FOLDERS=("Exceptions" "Interfaces" "Tests" "ValueObjects") #mozna dodac inne katalogi z katalogu glownego
    for folder in ${FOLDERS[@]}
    do
        printf "\n\nSprawdzam folder $folder\n\n";
        phpCheckCodeSyntax $folder
        phpCheckCodeForPSR2 $folder
        phpCheckCodeWithMessDetector $folder
    done
else
    if [ -d "$1" ]
    then
        phpCheckCodeSyntax $1
        phpCheckCodeForPSR2 $1
        phpCheckCodeWithMessDetector $1
    elif [ -f "$1" ]
    then
        start=$(date +%s)
        printf "Sprawdzam plik: $1\n"
        printf "Rozpoczynam sprawdzanie składni kodu za pomocą wywołania skryptu php -l\n";
        php -l $1 | grep 'Errors parsing' >> phpunitresult
        printf "Rozpoczynam sprawdzanie składni kodu za pomocą wywołania skryptu phpcs wg standardu PSR2\n";
        phpcs $1 --standard=PSR2 --extensions=php --error-severity=1 --warning-severity=8 >> phpunitresult
        printf "Rozpoczynam sprawdzanie składni kodu za pomocą wywołania skryptu phpmd\n";
        phpmd $1 text $cwd/phpmd.xml >> phpmdresult
    else
        echo "Podano błędny argument"
        rm -f phpunitresult
        exit 1
    fi
fi

printf  "PRZYGOTOWANIE TESTOW JEDNOSTKOWYCH\n"
phpunit -c  phpunit.xml.dist | tee -a phpunitresult
if [ -n "`grep "FAILURES" phpunitresult``grep ERROR phpunitresult`" ] || [ -s phpmdresult ]; then
    printf  "\n\nWystąpiły błędy w testach\n\n"
    rm -f phpunitresult
    rm -f phpmdresult
    exit 1
fi
printf  "TESTY JEDNOSTKOWE POPRAWNE\n\n"

rm -f phpunitresult
rm -f phpmdresult
