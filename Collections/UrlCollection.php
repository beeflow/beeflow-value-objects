<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 09.03.18
 * Time: 23:03
 */

namespace Beeflow\ValueObject\Collections;

use Beeflow\ValueObject\Abstracts\AbstractCollection;
use Beeflow\ValueObject\Exceptions\KeyInUseException;
use Beeflow\ValueObject\Interfaces\CollectionInterface;
use Beeflow\ValueObject\ValueObjects\Url;

class UrlCollection extends AbstractCollection implements CollectionInterface
{
    /**
     * Dodawanie elementu do kolekcji
     *
     * @param Mixed $obj
     * @param null  $key
     *
     * @return CollectionInterface
     * @throws KeyInUseException
     * @throws \TypeError
     */
    public function add($obj, $key = null)
    {
        if (!$obj instanceof Url) {
            throw new \TypeError("Invalid object type");
        }

        if ($key == null) {
            $this->items[] = $obj;

            return $this;
        }

        if (isset($this->items[ $key ])) {
            throw new KeyInUseException("Key $key already in use.");
        }

        $this->items[ $key ] = $obj;

        return $this;
    }

    /**
     * @param $key
     * @param $item
     *
     * @return $this
     * @throws \TypeError
     */
    public function set($item, $key)
    {
        if (!$item instanceof Url) {
            throw new \TypeError("Invalid object type");
        }

        $this->items[ $key ] = $item;

        return $this;
    }
}
