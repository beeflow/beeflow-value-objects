<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Lib;


class ArrayRequiredValidate
{
    /**
     * Sprawdzanie, czy w tablicy są wszystkie wymagane pola.
     * Jeżeli jeden z elementów listy będzie tablicą oznacza to, że na tej liście znajdują się klucze względnie
     * obowiązkowe.
     *
     * @param array $sourceArray    Dane przychodzące od użytkownika
     * @param array $requiredFields Lista pół, które są polami wymaganymi.
     *
     * @return bool
     */
    public function hasAllRequired(array $sourceArray, array $requiredFields): bool
    {
        $requestParamsKeys = array_keys($sourceArray);

        foreach ($requiredFields as $value) {
            if (\is_array($value)) {
                if ($this->checkOneOfRequired($sourceArray, $value)) {
                    continue;
                }

                return false;
            }

            if (!\in_array($value, $requestParamsKeys) || empty($sourceArray[ $value ])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Sprawdza, czy chociaż jedno z wymaganych pól jest na liście.
     * Pozwala to na wskazanie listy kluczy dla pól, z których chociaż jedno musi zostać wypełnione
     *
     * @param array $sourceArray
     * @param array $requiredFields
     *
     * @return bool
     */
    public function checkOneOfRequired(array $sourceArray, array $requiredFields): bool
    {
        $requestParamsKeys = array_keys($sourceArray);

        foreach ($requiredFields as $key) {
            if (\in_array($key, $requestParamsKeys) && isset($sourceArray[ $key ]) && !empty($sourceArray[ $key ])) {
                return true;
            }
        }

        return false;
    }
}