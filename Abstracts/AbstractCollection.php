<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 09.03.18
 * Time: 23:01
 */

namespace Beeflow\ValueObject\Abstracts;

use ArrayIterator;
use Beeflow\ValueObject\Exceptions\InvalidKeyException;
use Beeflow\ValueObject\Exceptions\KeyInUseException;
use Beeflow\ValueObject\Interfaces\CollectionInterface;


abstract class AbstractCollection implements CollectionInterface
{
    /**
     * @var array
     */
    protected $items = array();

    /**
     * Dodawanie elementu do kolekcji
     *
     * @param Mixed $obj
     * @param null  $key
     *
     * @return CollectionInterface
     * @throws KeyInUseException
     * @throws \TypeError
     */
    abstract public function add($obj, $key = null);

    /**
     * @param $key
     * @param $item
     *
     * @return $this
     * @throws \TypeError
     */
    abstract public function set($item, $key);

    /**
     * Usuwanie elementu z kolekcji
     *
     * @param $key
     *
     * @return CollectionInterface
     * @throws InvalidKeyException
     */
    public function remove($key)
    {
        if (!isset($this->items[ $key ])) {
            throw new InvalidKeyException("Invalid key $key.");
        }

        unset($this->items[ $key ]);

        return $this;
    }

    /**
     * Pobieranie elementu z kolekcji
     *
     * @param $key
     *
     * @return mixed
     * @throws InvalidKeyException
     */
    public function get($key)
    {
        if (!isset($this->items[ $key ])) {
            throw new InvalidKeyException("Invalid key $key.");
        }

        return $this->items[ $key ];
    }

    /**
     * @param null $flags
     *
     * @return $this
     */
    public function asort($flags = null)
    {
        asort($this->items, $flags);

        return $this;
    }

    //Sorting the array by keys
    public function ksort($flags = null)
    {
        ksort($this->items, $flags);

        return $this;
    }

    //Sorting the array naturally
    public function sort($flags = null)
    {
        sort($this->items, $flags);

        return $this;
    }

    /**
     * @return array
     */
    public function keys(): array
    {
        return array_keys($this->items);
    }

    /**
     * @return int
     */
    public function length(): int
    {
        return count($this->items);
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function keyExists($key): bool
    {
        return isset($this->items[ $key ]);
    }

    /**
     * Retrieve an external iterator
     *
     * @link  http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return ArrayIterator
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    /**
     * Return the current element
     *
     * @link  http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return current($this->items);
    }

    /**
     * Move forward to next element
     *
     * @link  http://php.net/manual/en/iterator.next.php
     * @since 5.0.0
     * @return Mixed
     */
    public function next()
    {
        return next($this->items);
    }

    /**
     * Return the key of the current element
     *
     * @link  http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return key($this->items);
    }

    /**
     * Checks if current position is valid
     *
     * @link  http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid(): bool
    {
        return (!is_null($this->key()));
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link  http://php.net/manual/en/iterator.rewind.php
     * @since 5.0.0
     */
    public function rewind()
    {
        return reset($this->items);
    }

    /**
     * @link http://php.net/manual/en/function.prev.php
     * @return mixed
     */
    public function previous()
    {
        return prev($this->items);
    }

    /**
     * Whether a offset exists
     *
     * @link  http://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset <p>
     *                      An offset to check for.
     *                      </p>
     *
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return $this->keyExists($offset);
    }

    /**
     * Offset to retrieve
     *
     * @link  http://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset <p>
     *                      The offset to retrieve.
     *                      </p>
     *
     * @return mixed Can return all value types.
     * @since 5.0.0
     * @throws InvalidKeyException
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * Offset to set
     *
     * @link  http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset <p>
     *                      The offset to assign the value to.
     *                      </p>
     * @param mixed $value  <p>
     *                      The value to set.
     *                      </p>
     *
     * @return CollectionInterface
     * @since 5.0.0
     * @throws \TypeError
     */
    public function offsetSet($offset, $value)
    {
        return $this->set($value, $offset);
    }

    /**
     * Offset to unset
     *
     * @link  http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset <p>
     *                      The offset to unset.
     *                      </p>
     *
     * @return CollectionInterface
     * @throws InvalidKeyException
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        return $this->remove($offset);
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    public function toArray(): array
    {
        $response = [];

        /** @var ValueObjectInterface $item */
        foreach ($this->items as $item) {
            $response[] = $item->get();
        }

        return $response;
    }
}
