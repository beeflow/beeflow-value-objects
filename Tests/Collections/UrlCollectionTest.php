<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 09.03.18
 * Time: 23:18
 */

namespace Beeflow\ValueObject\Tests\Collections;

use Beeflow\ValueObject\Collections\UrlCollection;
use Beeflow\ValueObject\ValueObjects\Email;
use Beeflow\ValueObject\ValueObjects\Url;
use PHPUnit\Framework\TestCase;

class UrlCollectionTest extends TestCase
{
    /**
     * @var UrlCollection;
     */
    protected $urlCollection;

    public function setUp()
    {
        $url1 = new Url('https://wp.pl');
        $url2 = new Url('https://onet.pl');
        $url3 = new Url('https://interia.pl');

        $this->urlCollection = new UrlCollection();
        $this->urlCollection->add($url1, 'test_provider');
        $this->urlCollection->add($url2, 'second_provider');
        $this->urlCollection->add($url3, 'third_provider');
    }

    public function testCanAddValidObjectToCollection()
    {
        $url = new Url('https://wp.pl');

        $urlCollection = new UrlCollection();
        $urlCollection->add($url);

        $this->assertEquals(1, $urlCollection->count());
    }

    /**
     * @expectedException \TypeError
     */
    public function testCannotAddInvalidObject()
    {
        $urlCollection = new UrlCollection();
        $urlCollection->add(123);
    }

    /**
     * @expectedException \Beeflow\ValueObject\Exceptions\KeyInUseException
     */
    public function testCannotAddUrlToExistedKey()
    {
        $url = new Url('https://wp.pl');

        $urlCollection = new UrlCollection();
        $urlCollection->add($url, 123);
        $urlCollection->add($url, 123);
    }

    public function testSetValidUrlObject()
    {
        $url = new Url('https://wp.pl');

        $urlCollection = new UrlCollection();
        $urlCollection->set($url, 123);

        $this->assertEquals(1, $urlCollection->count());
    }

    /**
     * @expectedException  \TypeError
     */
    public function testCannotSetInvalidObject()
    {
        $urlCollection = new UrlCollection();
        $urlCollection->set(123, 123);
    }

    public function testOffsetSet()
    {
        $url = new Url('https://wp.pl');

        $urlCollection = new UrlCollection();
        $urlCollection->offsetSet(123, $url);

        /** @var Url $actualUrl */
        $actualUrl = $urlCollection->get(123);

        $this->assertEquals('https://wp.pl', $actualUrl->get());
    }

    public function testCanSetProviderObjectToCollectionToSpecifiedKey()
    {
        $url = new Url('https://wp.pl');

        $urlCollection = new UrlCollection();
        $urlCollection->set($url, 'test');

        $actualUrl = $urlCollection->get('test');

        $this->assertEquals('https://wp.pl', $actualUrl->get());
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage Invalid object type
     */
    public function testSetWithInvalidObjectType()
    {
        $url = new Email('test@test.pl');
        $urlCollection = new UrlCollection();
        $urlCollection->set($url, 'test');
    }

    /**
     * @expectedException  \Beeflow\ValueObject\Exceptions\InvalidKeyException
     */
    public function testRemove()
    {
        $this->urlCollection->remove('test_provider')
            ->get('test_provider');
    }

    /**
     * @expectedException  \Beeflow\ValueObject\Exceptions\InvalidKeyException
     */
    public function testRemoveWithInvalidKey()
    {
        $this->urlCollection->remove('unknown_provider');
    }

    public function testGet()
    {
        $url = $this->urlCollection->get('test_provider');

        $this->assertTrue($url instanceof Url);
    }

    public function testLength()
    {
        $expected = 3;
        $actual = $this->urlCollection->length();

        $this->assertEquals($expected, $actual);
    }

    public function testCount()
    {
        $expected = 3;
        $actual = $this->urlCollection->count();

        $this->assertEquals($expected, $actual);
    }

    public function testOffsetExists()
    {
        $this->assertTrue(
            $this->urlCollection->offsetExists('test_provider')
        );
    }

    public function testKeyExists()
    {
        $this->assertTrue(
            $this->urlCollection->offsetExists('test_provider')
        );
    }

    public function testOffsetNotExists()
    {
        $this->assertFalse(
            $this->urlCollection->offsetExists('unknown_provider')
        );
    }

    public function testKeyNotExists()
    {
        $this->assertFalse(
            $this->urlCollection->offsetExists('unknown_provider')
        );
    }

    public function testIfOffsetIsInstanceOfProviderObject()
    {
        $this->assertTrue(
            $this->urlCollection->offsetGet('test_provider') instanceof Url
        );
    }

    public function testRewind()
    {
        $expectedUrl = 'https://wp.pl';
        $url = $this->urlCollection->rewind();
        $this->assertEquals($expectedUrl, $url->get());
    }

    public function testCurrent()
    {
        $expectedUrl = 'https://wp.pl';
        $url = $this->urlCollection->current();
        $this->assertEquals($expectedUrl, $url->get());
    }

    public function testNext()
    {
        $expectedUrl = 'https://onet.pl';
        $this->urlCollection->rewind();
        $url = $this->urlCollection->next();
        $this->assertEquals($expectedUrl, $url->get());
    }

    public function testKeyOfTheCurrentElement()
    {
        $expectedUrlKey = 'test_provider';
        $this->urlCollection->rewind();
        $actualKey = $this->urlCollection->key();

        $this->assertEquals($expectedUrlKey, $actualKey);
    }

    public function testIfKeyOfTheCurrentElementIsValid()
    {
        $this->urlCollection->rewind();
        $isCurrentKeyValid = $this->urlCollection->valid();

        $this->assertTrue($isCurrentKeyValid);
    }

    public function testIfKeyOfTheCurrentElementIsInvalid()
    {
        $this->urlCollection->rewind();
        $this->urlCollection->previous();
        $isCurrentKeyValid = $this->urlCollection->valid();

        $this->assertFalse($isCurrentKeyValid);
    }

    public function testUnsetOffset()
    {
        $expectedUrl = 'https://onet.pl';
        $url = new Url($expectedUrl);
        $this->urlCollection->add($url, 'to_unset');

        $urlFromCollection = $this->urlCollection->get('to_unset');
        $this->assertEquals($url, $urlFromCollection->get());

        $this->urlCollection->offsetUnset('to_unset');

        $this->assertFalse(
            $this->urlCollection->offsetExists('to_unset')
        );
    }

    /**
     * @expectedException \Beeflow\ValueObject\Exceptions\InvalidKeyException
     */
    public function testUnsetInvalidOffset()
    {
        $this->urlCollection->offsetUnset('another_one_to_unset');
    }

    public function testIterator()
    {
        foreach ($this->urlCollection as $url) {
            $this->assertTrue($url instanceof Url);
        }
    }


    public function testAsort()
    {
        $expectedUrl = 'https://onet.pl';

        $this->urlCollection->asort()->rewind();
        $url = $this->urlCollection->next();

        $this->assertEquals($expectedUrl, $url->get());
    }

    public function testKsort()
    {
        $expectedUrl = 'https://wp.pl';

        $this->urlCollection->ksort()->rewind();
        $url = $this->urlCollection->next();

        $this->assertEquals($expectedUrl, $url->get());
    }

    public function testSort()
    {
        $expectedUrl = 'https://onet.pl';

        $this->urlCollection->sort()->rewind();
        $url = $this->urlCollection->next();

        $this->assertEquals($expectedUrl, $url->get());
    }

    public function testGetKeys()
    {
        $expectedKeys = array(
            'test_provider', 'second_provider', 'third_provider', 'to_test'
        );

        $url = new Url('https://beeflow.com.pl');
        $this->urlCollection->add($url, 'to_test');

        $actualKeys = $this->urlCollection->keys();

        $this->assertEquals($expectedKeys, $actualKeys);
    }

    public function testToArray()
    {
        $expected = [
            'https://wp.pl',
            'https://onet.pl',
            'https://interia.pl'
        ];

        $this->assertEquals($expected, $this->urlCollection->toArray());
    }
}
