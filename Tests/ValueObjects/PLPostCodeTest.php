<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\PLPostCode;
use PHPUnit\Framework\TestCase;

class PLPostCodeTest extends TestCase
{

    public function testIfCanCreateObjectWithCorrectPostCode()
    {
        $expected = '78-200';
        $postCode = new PLPostCode($expected);

        $this->assertEquals($expected, (string)$postCode);
        $this->assertEquals($expected, $postCode->get());
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfCannotCreateWithIncorrectLength()
    {
        new PLPostCode('78-20');
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfCannotCreateWithIncorrectCharacters()
    {
        new PLPostCode('78-20A');
    }

    /**
     * @expectedException \Beeflow\ValueObject\Exceptions\CastException
     */
    public function testIfCannotConvertToInt()
    {
        (new PLPostCode('78-200'))->toInt();
    }
}
