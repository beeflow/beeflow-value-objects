<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 10.03.18
 * Time: 08:39
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\PhoneNumber;
use PHPUnit\Framework\TestCase;

class PhoneNumberTest extends TestCase
{
    /**
     * @return array
     */
    public function wrongPhoneNumberProvider(): array
    {
        return [
            ['absc'],
            ['ASD@aaa'],
            [''],
        ];
    }

    /**
     * @dataProvider wrongPhoneNumberProvider
     *
     * @expectedException \TypeError
     *
     * @param string $phoneNumber
     */
    public function testWrongPhoneNumberException(string $phoneNumber): void
    {
        new PhoneNumber($phoneNumber);
    }

    /**
     * @dataProvider wrongPhoneNumberTypeProvider
     *
     * @expectedException \TypeError
     *
     * @param string $phoneNumber
     */
    public function testWrongPhoneNumberTypeError(string $phoneNumber) : void
    {
        new PhoneNumber($phoneNumber);
    }

    /**
     * @return array
     */
    public function wrongPhoneNumberTypeProvider() : array
    {
        return [
            ['11/28/1989'],
            ['1122.00'],
            ['1122'],
            ['1122-00'],
            ['48 22 790 790 790'],
            ['22 790/790/790'],
            ['22/790/790/790'],
            ['1112233'],
            ['0777222333']
        ];
    }

    /**
     *
     * @dataProvider successPhoneNumber
     *
     * @param string $phoneNumber
     * @param string $expected
     *
     * @throws \TypeError
     */
    public function testSuccessPhoneNumber(string $phoneNumber, string $expected) : void
    {
        $number = new PhoneNumber($phoneNumber);
        $this->assertEquals($expected, $number->get());
    }

    /**
     *
     * @dataProvider successPhoneNumber
     *
     * @param string $phoneNumber
     * @param string $expected
     *
     * @throws \TypeError
     */
    public function testSuccessPhoneNumberAsString(string $phoneNumber, string $expected) : void
    {
        $number = new PhoneNumber($phoneNumber);
        $this->assertEquals($expected, $number);
    }

    /**
     * @return array
     */
    public function successPhoneNumber() : array
    {
        return [
            ['48 790 790 790', '+48 790 790 790'],
            ['790 790 790', '+48 790 790 790'],
            ['790-790-790', '+48 790 790 790'],
            ['790/790/790', '+48 790 790 790'],
            ['48581112233', '+48 58 111 22 33'],
            ['48777222333', '+48 77 722 23 33'],
            ['581112233', '+48 58 111 22 33'],
            ['777222333', '+48 77 722 23 33'],
            ['0048581112233', '+48 58 111 22 33']
        ];
    }
}
