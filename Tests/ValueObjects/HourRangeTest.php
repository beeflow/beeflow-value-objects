<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 20.04.18
 * Time: 21:15
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\Hour;
use Beeflow\ValueObject\ValueObjects\HourRange;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HourRangeTest extends WebTestCase
{
    public function testIfRangeReturnsCorrectValueWhenItIsNotInOneDay()
    {
        $hourStart = new Hour('11:12');
        $hourStop = new Hour('14:12');

        $range = new HourRange($hourStop, $hourStart);

        $this->assertEquals($range->toInt(), -300);
    }

    public function testIfRangeReturnsCorrectValueWhenItIsInOneDay()
    {
        $hourStart = new Hour('11:12');
        $hourStop = new Hour('14:12');

        $range = new HourRange($hourStart, $hourStop);

        $this->assertEquals($range->toInt(), 300);
    }
}
