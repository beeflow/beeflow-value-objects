<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\HourWithMinutes;
use Beeflow\ValueObject\ValueObjects\HourWithMinutesRange;
use PHPUnit\Framework\TestCase;

class HourWithMinutesRangeTest extends TestCase
{
    public function testIfToIntReturnsCorrectValue()
    {
        $expected = 60;

        $hourStart = new HourWithMinutes('10:00');
        $hourStop = new HourWithMinutes('11:00');

        $hourRange = new HourWithMinutesRange($hourStart, $hourStop);

        $actual = $hourRange->toInt();

        $this->assertEquals($expected, $actual);
    }
}
