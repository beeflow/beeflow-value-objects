<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\Id;
use PHPUnit\Framework\TestCase;

class IdTest extends TestCase
{
    public function testIfCanCreateObjectWithCorrectId()
    {
        $id = new Id(123);
        $this->assertEquals("123", (string)$id);
    }

    public function testIfThrowsTypeErrorWhenIdIsLessThanZero()
    {
        $this->expectException(\TypeError::class);
        new Id(-111);
    }

    public function testIfThrowsTypeErrorWhenIdHasNonNumericCharacter()
    {
        $this->expectException(\TypeError::class);
        new Id('a123#bc');
    }

    public function testIfThrowsTypeErrorWhenIdIsEmpty()
    {
        $this->expectException(\TypeError::class);
        new Id();
    }

    public function testIfThrowsTypeErrorWhenIdIsEqualZero()
    {
        $this->expectException(\TypeError::class);
        new Id(0);
    }
}
