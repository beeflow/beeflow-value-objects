<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\HourWithMinutes;
use PHPUnit\Framework\TestCase;

class HourWithMinutesTest extends TestCase
{
    public function testIfToIntReturnsCorrectMinutes()
    {
        $expected = 620;
        $hour = new HourWithMinutes('10:20');
        $actual = $hour->toInt();

        $this->assertEquals($expected, $actual);
    }
}
