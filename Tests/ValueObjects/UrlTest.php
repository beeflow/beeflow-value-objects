<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 09.03.18
 * Time: 18:26
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\Url;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{
    /**
     * @expectedException \TypeError
     */
    public function testCannotCreateWithIncorectUrl()
    {
        new Url('test@test.pl');
    }

    public function testCanCreateWithValidErl()
    {
        $expected = 'http://www.wp.pl';
        $url = new Url($expected);

        $this->assertEquals($expected, $url->get());
    }

    public function testToString()
    {
        $expected = 'http://www.wp.pl';
        $url = new Url($expected);

        $this->assertEquals($expected, (string)$url);
    }
}
