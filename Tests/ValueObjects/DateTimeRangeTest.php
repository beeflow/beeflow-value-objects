<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\DateTimeRange;
use PHPUnit\Framework\TestCase;

class DateTimeRangeTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testIfCanBuildObjectWithCorrectData()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");
        $dataStop = new \DateTime("2018-01-01 11:11");

        $dateRange = new DateTimeRange($dataStart, $dataStop);
        list ($actualDataStart, $actualDataStop) = $dateRange->get();

        $this->assertEquals($dataStart, $actualDataStart);
        $this->assertEquals($actualDataStop, $dataStop);
    }

    /**
     * @throws \Exception
     */
    public function testIfCanBuildObjectWithEmptyEndData()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");

        $dateRange = new DateTimeRange($dataStart);
        list ($actualDataStart, $actualDataStop) = $dateRange->get();

        $this->assertEquals($dataStart, $actualDataStart);
        $this->assertNull($actualDataStop);
    }

    /**
     * @throws \Exception
     * @expectedException \TypeError
     */
    public function testIfThrowExceptionOnIncorrectEndDateType()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");
        $dataStop = "2018-01-01 11:11";

        new DateTimeRange($dataStart, $dataStop);
    }

    /**
     * @throws \Exception
     */
    public function testIfReturnsCorrectStringRangeWithEmptyEndDate()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");

        $dateRange = new DateTimeRange($dataStart);

        $this->assertEquals(
            $dataStart->format('Y-m-d H:i') . ' - ...',
            (string)$dateRange
        );
    }

    /**
     * @throws \Exception
     */
    public function testIfReturnsCorrectStringRange()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");
        $dataStop = new \DateTime("2018-01-01 11:11");

        $dateRange = new DateTimeRange($dataStart, $dataStop);

        $this->assertEquals(
            $dataStart->format('Y-m-d H:i') . ' - ' . $dataStop->format('Y-m-d H:i'),
            (string)$dateRange
        );
    }

    /**
     * @throws \Exception
     * @expectedException \TypeError
     */
    public function testIfCannotBuildObjectWithLowerEndDate()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");
        $dateStop = new \DateTime('2018-01-01-11:09');

        new DateTimeRange($dataStart, $dateStop);
    }


    /**
     * @throws \Exception
     * @expectedException \TypeError
     */
    public function testIfCannotBuildObjectWithTheSameDates()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");

        new DateTimeRange($dataStart, $dataStart);
    }

    /**
     * @throws \Exception
     * @expectedException \Beeflow\ValueObject\Exceptions\CastException
     */
    public function testIfThrowsCastExceptionWithToIntMethod()
    {
        $dataStart = new \DateTime("2018-01-01 11:10");
        $dataStop = new \DateTime("2018-01-01 11:11");

        $dateRange = new DateTimeRange($dataStart, $dataStop);
        $dateRange->toInt();
    }
}
