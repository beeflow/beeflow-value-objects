<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\Pesel;
use PHPUnit\Framework\TestCase;

class PeselTest extends TestCase
{
    public function testIfCanCreateCorrectPesel()
    {
        $expected = '78051303038';
        $pesel = new Pesel($expected);

        $this->assertEquals($expected, $pesel->get());
        $this->assertEquals($expected, (string)$pesel);
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfIncorrectLengthThrowsTypeError()
    {
        new Pesel('7805130303');
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfIncorrectCharactersThrowsTypeError()
    {
        new Pesel('78O51303038');
    }

    /**
     * @expectedException \Beeflow\ValueObject\Exceptions\CastException
     */
    public function testIfCastToIntThrowsCastException()
    {
        $expected = '78051303038';
        $pesel = new Pesel($expected);

        $pesel->toInt();
    }
}
