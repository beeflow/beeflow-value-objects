<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 20.04.18
 * Time: 21:09
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\ValueObjects\Hour;
use Beeflow\ValueObject\ValueObjects\HourRangeInDay;
use PHPUnit\Framework\TestCase;

class HourRangeInDayTest extends TestCase
{
    public function testIfDateHourRangeIsCorrect()
    {
        $hourStart = new Hour('11:12');
        $hourStop = new Hour('14:12');
        try {
            $range = new HourRangeInDay($hourStart, $hourStop);
        } catch (CastException $e) {
            $this->assertTrue(false);
        }

        $this->assertEquals($range->toInt(), 300);
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfIncorrectRangeThrowsTypeError()
    {
        $hourStart = new Hour('11:12');
        $hourStop = new Hour('14:12');

        new HourRangeInDay($hourStop, $hourStart);
    }
}
