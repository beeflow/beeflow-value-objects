<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\Username;
use PHPUnit\Framework\TestCase;

class UsernameTest extends TestCase
{
    public function testIfCanCreateWithCorrectUserName()
    {
        $expected = 'rprzetakowski';
        $username = new Username($expected);

        $this->assertEquals($expected, $username->get());
        $this->assertEquals($expected, (string)$username);
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfCannotCreateObjectWithTooShortUsername()
    {
        new Username('a');
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfCannotCreateObjectWithIncorrectCharacters()
    {
        new Username('użytkownik');
    }

    /**
     * @expectedException \TypeError
     */
    public function testIfCannotCreateObjectWithSpaceInUsername()
    {
        new Username('u sername');
    }

    /**
     * @expectedException \Beeflow\ValueObject\Exceptions\CastException
     */
    public function testIfCannotConvertToInt()
    {
        $username = new Username('rprzetakowski');

        $username->toInt();
    }
}
