<?php

/**
 * @copyright 2019 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\ValueObjects\PositiveNumberWithMaxValue;
use PHPUnit\Framework\TestCase;
use UnexpectedValueException;

class PositiveNumberWithMaxValueTest extends TestCase
{
    public function testIfThrowExceptionWithTooBigValue(): void
    {
        $this->expectException(UnexpectedValueException::class);
        new PositiveNumberWithMaxValue(100, 10);
    }
}