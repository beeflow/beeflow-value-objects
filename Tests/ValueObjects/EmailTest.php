<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 03.03.18
 * Time: 16:19
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use ArgumentCountError;
use Beeflow\ValueObject\ValueObjects\Email;
use PHPUnit\Framework\TestCase;
use TypeError;

class EmailTest extends TestCase
{

    public function testToString()
    {
        $addressEmail = 'some@o2.pl';
        $email = new Email($addressEmail);

        $this->assertEquals($email->toString(), $addressEmail);
    }

    /**
     * @expectedException TypeError
     */
    public function testSetIncorrectEmail()
    {
        new Email('some#test.pl');
    }

    /**
     * @expectedException TypeError
     */
    public function testIfThrowTypeErrorWhenEmailHasHash()
    {
        new Email('so#me@test.pl');
    }

    /**
     * @expectedException ArgumentCountError
     */
    public function testIncorrectSet()
    {
        new Email();
    }

    public function testGet()
    {
        $addressEmail = 'some@test.pl';
        $email = new Email($addressEmail);

        $this->assertEquals($email->get(), $addressEmail);
    }
}
