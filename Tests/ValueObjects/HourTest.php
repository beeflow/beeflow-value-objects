<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 20.04.18
 * Time: 20:15
 */

namespace Beeflow\ValueObject\Tests\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\ValueObjects\Hour;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HourTest extends WebTestCase
{
    /**
     * @expectedException \TypeError
     */
    public function testIfIncorrectHourThrowsTypeError()
    {
        $incorrectHour = '25:11';
        new Hour($incorrectHour);
    }

    /**
     * Sprawdza, czy utworzono prawidłowy obiekt
     *
     * @throws CastException
     */
    public function testIfToIntMethodCreateReturnsCorectValue()
    {
        $correctHour = '22:11';
        $expected = 2211;
        $hour = new Hour($correctHour);

        $this->assertEquals($hour->toInt(), $expected);
    }

    public function testIfGetMethodReturnsCorectValue()
    {
        $correctHour = '22:11';
        $hour = new Hour($correctHour);

        $this->assertEquals($hour->get(), $correctHour);
    }

    public function testIfToStringReturnsCorectValue()
    {
        $correctHour = '22:11';
        $hour = new Hour($correctHour);

        $this->assertEquals((string)$hour, $correctHour);
    }
}
