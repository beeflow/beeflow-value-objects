<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 09.03.18
 * Time: 22:18
 */

namespace Beeflow\ValueObject\Tests;

use Beeflow\ValueObject\Lib\ArrayRequiredValidate;
use PHPUnit\Framework\TestCase;

class ArrayRequiredValidateTest extends TestCase
{
    /**
     * @var ArrayRequiredValidate
     */
    protected $arrayRequireValidator;

    /**
     * Ustawiam validator
     */
    public function setUp()
    {
        $this->arrayRequireValidator = new ArrayRequiredValidate();
    }

    /**
     * Sprawdzam, czy metoda zwróci false, gdy na liście nie będzie jednego z wymaganych elementów
     */
    public function testHasNotAllRequired()
    {
        $elements = ['one' => 1, 'two' => 2];
        $hasAllRequired = $this->arrayRequireValidator->hasAllRequired($elements, ['tree']);

        $this->assertFalse($hasAllRequired);
    }

    /**
     * Sprawdza, czy wszystkie elementy są na liście
     */
    public function testHasAllRequired()
    {
        $elements = ['one' => 1, 'two' => 2, 'tree' => 3];
        $hasAllRequired = $this->arrayRequireValidator->hasAllRequired($elements, ['two', 'tree']);

        $this->assertTrue($hasAllRequired);
    }

    /**
     * Test występowania jednego z opcjonalnych elementów
     */
    public function testCheckOneOfRequired()
    {
        $elements = ['one' => 1, 'tree' => 3];
        $hasAllRequired = $this->arrayRequireValidator->checkOneOfRequired($elements, ['two', 'tree']);

        $this->assertTrue($hasAllRequired);
    }

    /**
     * Test braku wszystkich opcjonalnych elementów
     */
    public function testCheckOneOfRequiredNotExists()
    {
        $elements = ['one' => 1, 'four' => 3];
        $hasAllRequired = $this->arrayRequireValidator->checkOneOfRequired($elements, ['two', 'tree']);

        $this->assertFalse($hasAllRequired);
    }

    /**
     * Testuje, czy poprawnie zostaną sprawdzone wszystkie elementy wymagane i opcjonalne
     */
    public function testCheckAllRequiredWithOptional()
    {
        $elements = ['one' => 1, 'two' => 2, 'tree' => 3, 'four' => 4];
        $hasAllRequired = $this->arrayRequireValidator->hasAllRequired($elements, ['one', ['tree', 'four']]);

        $this->assertTrue($hasAllRequired);
    }

    /**
     * Testuje, czy poprawnie zostaną sprawdzone wszystkie elementy wymagane i opcjonalne w przypadku braku opcjonalnych
     */
    public function testCheckAllRequiredWithoutOptional()
    {
        $elements = ['one' => 1, 'two' => 2];
        $hasAllRequired = $this->arrayRequireValidator->hasAllRequired($elements, ['one', ['tree', 'four']]);

        $this->assertFalse($hasAllRequired);
    }
}
