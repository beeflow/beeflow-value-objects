<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class Id implements ValueObjectInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * Hour constructor.
     *
     * @param string $id as HH:mm
     *
     * @throws \TypeError
     */
    public function __construct(int $id)
    {
        if (!preg_match('/^\d/', $id) || $id <= 0) {
            throw new \TypeError('Incorrect Id type');
        }

        $this->id = $id;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->id;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     */
    public function toInt(): int
    {
        return $this->id;
    }
}
