<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;


class PositiveNumber extends Id
{
    public function __construct($id)
    {
        try {
            parent::__construct($id);
        } catch (\TypeError $ex) {
            throw new \TypeError('Incorrect PositiveNumber type');
        }
    }
}