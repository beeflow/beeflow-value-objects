<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class Pesel implements ValueObjectInterface
{

    protected $value;

    public function __construct(string $pesel)
    {
        if (!preg_match('/^\d{11}$/', $pesel)) {
            throw new \TypeError('PESEL is incorrect');
        }

        $weights = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
        $sum = 0;

        for ($i = 0; $i < 10; $i++) {
            $sum += (int)$pesel[$i] * $weights[$i];
        }

        $sumModuloTen = $sum % 10;
        $controlElement = 10 - $sumModuloTen;

        if ($controlElement != $pesel[10]) {
            throw new \TypeError('PESEL is incorrect');
        }

        $this->value = $pesel;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('You cannot convert PESEL to int');
    }
}
