<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

/**
 * Class DayNumber
 * Przyjmuje wartości od 1 - 7 (pn - nd)
 *
 * @package AppBundle\ValueObjects
 */
class DayNumber implements ValueObjectInterface
{

    /**
     * @var int
     */
    protected $dayNumber;

    /**
     * DayNumber constructor.
     *
     * @param int $dayNumber
     *
     * @throws \TypeError
     */
    public function __construct(int $dayNumber)
    {
        if ($dayNumber < 1 || $dayNumber > 7) {
            throw new \TypeError('Incorrect day number type');
        }

        $this->dayNumber = $dayNumber;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return (string)$this->dayNumber;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->dayNumber;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        return $this->dayNumber;
    }
}
