<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class Hour implements ValueObjectInterface
{
    /**
     * @var string
     */
    protected $hour;

    /**
     * Hour constructor.
     *
     * @param string $hour as HH:mm
     *
     * @throws \TypeError
     */
    public function __construct(string $hour)
    {
        if (!(preg_match("/[0-2][0-9]:[0-5][0-9]/", $hour))) {
            throw new \TypeError('Incorrect hour type');
        }

        $hourAsInt = (int)str_replace(":", "", $hour);

        if ($hourAsInt > 2359 || $hourAsInt < 0) {
            throw new \TypeError('Incorrect hour type');
        }

        $this->hour = $hour;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->hour;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->hour;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     */
    public function toInt(): int
    {
        return (int)str_replace(":", "", $this->hour);
    }
}
