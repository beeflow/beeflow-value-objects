<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

class HourWithMinutes extends Hour
{
    public function toInt(): int
    {
        list ($hour, $minutes) = explode(':', $this->hour);
        $minutesInHout = 60;

        return (int)$hour * $minutesInHout + (int)$minutes;
    }
}
