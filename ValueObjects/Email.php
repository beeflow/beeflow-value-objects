<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 03.03.18
 * Time: 16:09
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class Email implements ValueObjectInterface
{
    /**
     * @var string
     */
    protected $value;

    /**
     * Email constructor.
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]*)$/", $value)) {
            $this->value = $value;
        } else {
            throw new \TypeError('This is not correct email value');
        }
    }

    /**
     * Metoda zwraca bazową wartość obiektu w postaci ciągu znaków
     *
     * @return string
     */
    public function toString(): string
    {
        return (string)$this->value;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     *
     * @return string
     */
    public function get(): string
    {
        return (string)$this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('You cannot get Email as an int');
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->get();
    }
}
