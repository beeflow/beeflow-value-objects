<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

class HourWithMinutesRange extends HourRange
{
    public function toInt(): int
    {
        list($hourStart, $minutesStart) = explode(":", $this->hourStart);
        list($hourStop, $minutesStop) = explode(":", $this->hourStop);
        $minutesInHout = 60;

        $timeInMinutesStart = (int)$hourStart * $minutesInHout + (int)$minutesStart;
        $timeInMinutesStop = (int)$hourStop * $minutesInHout + (int)$minutesStop;

        return $timeInMinutesStop - $timeInMinutesStart;
    }
}
