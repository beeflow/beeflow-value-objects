<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class HourRange implements ValueObjectInterface
{

    /**
     * @var Hour
     */
    protected $hourStart;

    /**
     * @var Hour
     */
    protected $hourStop;

    /**
     * HourRange constructor.
     *
     * @param Hour $hourStart
     * @param Hour $hourStop
     */
    public function __construct(Hour $hourStart, Hour $hourStop)
    {
        $this->hourStart = $hourStart;
        $this->hourStop = $hourStop;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->hourStart->get() . ' - ' . $this->hourStop->get();
    }

    public function getHourStart(): Hour
    {
        return $this->hourStart;
    }

    public function getHourStop(): Hour
    {
        return $this->hourStop;
    }

    /**
     * @return mixed
     */
    public function get(): string
    {
        return $this->hourStart->get() . ' - ' . $this->hourStop->get();
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws
     */
    public function toInt(): int
    {
        return $this->hourStop->toInt() - $this->hourStart->toInt();
    }
}
