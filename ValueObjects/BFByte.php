<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class BFByte implements ValueObjectInterface
{

    /**
     * @var int
     */
    protected $value;

    /**
     * BFByte constructor.
     *
     * @param int $value
     */
    public function __construct(int $value)
    {
        if (!in_array($value, array(0, 1))) {
            throw new \TypeError('Value must be ' . __CLASS__ . ' type but is ' . gettype($value));
        }

        $this->value = $value;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return (string)$this->value;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        return $this->value;
    }
}
