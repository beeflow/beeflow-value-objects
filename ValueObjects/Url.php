<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class Url implements ValueObjectInterface
{

    protected $value;

    /**
     * Url constructor.
     *
     * @param string $url
     *
     * @throws \TypeError
     */
    public function __construct(string $url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \TypeError('Wrong URL format, tried data:[' . $url . ']');
        }

        $this->value = $url;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return (string)$this->value;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return (string)$this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('You cannot get Url as an int');
    }
}
