<?php
/**
 * Created by PhpStorm.
 * User: rprzetakowski
 * Date: 20.04.18
 * Time: 20:13
 */

namespace Beeflow\ValueObject\ValueObjects;

class HourRangeInDay extends HourRange
{
    /**
     * HourRange constructor.
     *
     * @param Hour $hourStart
     * @param Hour $hourStop
     *
     * @throws \TypeError
     */
    public function __construct(Hour $hourStart, Hour $hourStop)
    {
        if ($hourStart->toInt() > $hourStop->toInt()) {
            throw new \TypeError('Incorrect hour range');
        }

        parent::__construct($hourStart, $hourStop);
    }
}
