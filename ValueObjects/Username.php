<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class Username implements ValueObjectInterface
{

    protected $value;

    public function __construct(string $username, int $minimumLength = 3)
    {
        if (strlen($username) < $minimumLength) {
            throw new \TypeError('Username must have at least 3 characters');
        }

        if (!preg_match('/^([a-zA-Z0-9])+$/', $username)) {
            throw new \TypeError('Username has incorrect characters');
        }

        $this->value = $username;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('Cannot convert username to integer');
    }
}
