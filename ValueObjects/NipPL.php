<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class NipPL implements ValueObjectInterface
{
    /**
     *
     * @var string
     */
    protected $value;


    public function __construct(string $value)
    {
        $value = str_replace('-', '', $value);
        $countryCode = \substr($value, 0, 2);

        // w polsce domyślnie nie podaje się oznaczenia literowego kraju
        if (((integer)$countryCode) == 0 && strtoupper($countryCode) != 'PL') {
            throw new \TypeError('Value must be correct NipPL type');
        }

        $value = str_replace($countryCode, '', $value);

        if (strlen($value) <> 10) {
            throw new \TypeError('Value must be correct NipPL type');
        }

        $weights = '657234567';
        $sum = 0;

        for ($i = 0; $i <= 8; $i++) {
            $sum += (integer)$value[$i] * (integer)$weights[$i];
        }

        if ((integer)$value[9] == ($sum % 11)) {
            $this->value = $value;
        } else {
            throw new \TypeError('Value must be correct NipPL type');
        }
    }


    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('Cannot convert NipPL to integer');
    }
}
