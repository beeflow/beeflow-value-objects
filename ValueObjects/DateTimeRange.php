<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;
use DateInterval;

class DateTimeRange implements ValueObjectInterface
{

    /**
     * @var \DateTime
     */
    protected $dateStart;

    /**
     * @var \DateTime
     */
    protected $dateStop;

    /**
     * DateTimeRange constructor.
     *
     * @param \DateTime $dateStart
     * @param \DateTime $dateStop
     *
     * @throws \Exception
     */
    public function __construct(\DateTime $dateStart, $dateStop = null)
    {
        if (!empty($dateStop) && !($dateStop instanceof \DateTime)) {
            throw new \TypeError('Incorrect date range. End date must be a DateTime instance');
        }

        if ($dateStop instanceof \DateTime && $dateStart >= $dateStop) {
            throw new \TypeError('Incorrect date range. Date start cannot be higher than date stop');
        }

        $this->dateStart = $dateStart;
        $this->dateStop = $dateStop;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     *
     * @throws CastException
     */
    public function __toString()
    {
        $endDate = '...';
        if ($this->dateStop instanceof \DateTime) {
            $endDate = $this->dateStop->format('Y-m-d H:i');
        }

        return $this->dateStart->format('Y-m-d H:i') . ' - ' . $endDate;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return [
            $this->dateStart, $this->dateStop
        ];
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('Cannot convert DateTimeRange to integer');
    }
}
