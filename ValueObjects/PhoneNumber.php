<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class PhoneNumber implements ValueObjectInterface
{

    /**
     * @var PhoneNumberUtil
     */
    protected $phoneUtil;

    /**
     * @var \libphonenumber\PhoneNumber
     */
    protected $value;

    /**
     * PhoneNumber constructor.
     *
     * @param string $phoneNumber
     *
     * @throws \TypeError
     * @throws \libphonenumber\NumberParseException
     */
    public function __construct(string $phoneNumber)
    {
        try {
            $this->phoneUtil = PhoneNumberUtil::getInstance();
            $number = $this->phoneUtil->parse($phoneNumber, "PL");

            if (!$this->phoneUtil->isValidNumber($number)) {
                throw new \TypeError('Phone number is not valid, tried data:[' . $phoneNumber . ']');
            }

            $this->value = $number;
        } catch (NumberParseException $exception) {
            throw new \TypeError($exception->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->phoneUtil->format($this->value, PhoneNumberFormat::INTERNATIONAL);
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->get();
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        throw new CastException('You cannot get Phone number as an int');
    }
}
