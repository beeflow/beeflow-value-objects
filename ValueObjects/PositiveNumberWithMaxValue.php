<?php

/**
 * @copyright 2019 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use UnexpectedValueException;

class PositiveNumberWithMaxValue extends Id
{
    public function __construct(int $value, $maxValue)
    {
        try {
            parent::__construct($value);
        } catch (\TypeError $error) {
            throw new \TypeError('Incorrect PositiveNumber type');
        }

        if ($value > $maxValue) {
            throw new UnexpectedValueException('Value is too big');
        }

        $this->id = $value;
    }
}