<?php
/**
 * @copyright 2018 Beeflow Ltd
 * @author    Rafal Przetakowski <rafal.p@beeflow.co.uk>
 */

namespace Beeflow\ValueObject\ValueObjects;

use Beeflow\ValueObject\Exceptions\CastException;
use Beeflow\ValueObject\Interfaces\ValueObjectInterface;

class BFDateTime implements ValueObjectInterface
{
    public $value;

    public function __construct(\DateTime $value)
    {
        $this->value = $value;
    }

    /**
     * Metoda zwraca bazową wartość obiektu
     */
    public function __toString()
    {
        return $this->value->format('Y-m-d H:i:s');
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * Rzutuje wartość na int. Jeżeli wartość nie może być rzutowana, pojawia się wyjątek CastException
     *
     * @return int
     * @throws CastException
     */
    public function toInt(): int
    {
        return $this->value->getTimestamp();
    }
}
